﻿using Microsoft.EntityFrameworkCore;
using PhoneStoreManager.Database;
using PhoneStoreManager.Domain;
using PhoneStoreManager.EntityFramework;
using PhoneStoreManager.Repositories;
using PhoneStoreManager.Services;
using System;

namespace PhoneStoreManager.ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            var database = new InMemoryDatabase();
            var context = new PhoneStoreManagerDbContext(new DbContextOptionsBuilder().UseSqlServer(Constants.ConnectionString).Options);

            //InitializeManufacturers(database, context);

            //InitializeStores(database, context);


            InitializePhones(database,context);

            InitializeStorePhones(database, context);

            Console.ReadKey();
        }

        private static void InitializeStorePhones(InMemoryDatabase database, PhoneStoreManagerDbContext context)
        {
            //var StorePhones1 = new StorePhones
            //{
            //    StoreId = 1,
            //    PhoneId = 1,
            //    Price = 799
            //};
            //var StorePhones2 = new StorePhones
            //{
            //    StoreId = 2,
            //    PhoneId=1,
            //    Price=849
            //};
            //var StorePhones3 = new StorePhones
            //{
            //    StoreId = 3,
            //    PhoneId = 1,
            //    Price = 899
            //};
            //var StorePhones4 = new StorePhones
            //{
            //    StoreId = 2,
            //    PhoneId = 2,
            //    Price = 549
            //};
            //var StorePhones5 = new StorePhones
            //{
            //    StoreId = 3,
            //    PhoneId = 2,
            //    Price = 499
            //};
            //var StorePhones6 = new StorePhones
            //{
            //    StoreId = 3,
            //    PhoneId = 3,
            //    Price = 799
            //};
            //var adoRepository = new AdoStoreRepository();

            var phoneRepository = new EFPhoneRepository(context);
            var storeRepository = new EFStoreRepository(context);

            var phoneService = new PhoneService(phoneRepository);
            var storeService = new StoreService(storeRepository);

            var phones = phoneService.Search("samsung");
            var store = storeService.FindByName("SamsungStore");

            //if (store != null)
            //{
            //    foreach (var phone in phones)
            //    {
            //        storeService.AddPhoneToStore(store.Id, phone.Id, 400m);
            //    }
            //}


            //service.AddPhoneToStore(1, 1, 799);
            //service.AddPhoneToStore(2, 1, 849);
            //service.AddPhoneToStore(3, 1, 899);
            //service.AddPhoneToStore(2, 2, 549);
            //service.AddPhoneToStore(3, 2, 499);
            //service.AddPhoneToStore(3, 3, 799);
        }

        public static void InitializeManufacturers(InMemoryDatabase database, PhoneStoreManagerDbContext context)
        {
            var manufacturer1 = new Manufacturer
            {
                Id = 1,
                Name = "Samsung",
                Address = "South Korea, Street, Town"
            };

            var manufacturer2 = new Manufacturer
            {
                Id = 2,
                Name = "Apple",
                Address = "USA, Street, Town"
            };

            var manufacturer3 = new Manufacturer
            {
                Id = 3,
                Name = "Xiaomi",
                Address = "China, Street, Town"
            };

            var manufacturer4 = new Manufacturer
            {
                Id = 4,
                Name = "Huawei",
                Address = "China, Street23, TownT"
            };

            var manufacturer5 = new Manufacturer
            {
                Name = "LG",
                Address = "South Korea, Street, Town"
            };

            //var linqRepository = new LinqManufacturerRepository(database);
            //var customRepository = new CustomManufacturerRepository(database);
            //var adoRepository = new AdoManufacturerRepository();
            var efRepository = new EFManufacturerRepository(context);

            var service = new ManufacturerService(efRepository);

            //service.Add(manufacturer1);
            //service.Add(manufacturer2);
            //service.Add(manufacturer3);
            //service.Add(manufacturer4);
            //service.Add(manufacturer5);

            Manufacturer xiaomi = service.FindByName("Xiaomi");

            if (xiaomi != null)
            {
                Console.WriteLine($"Manufacturer name: {xiaomi.Name}");
            }
            else
            {
                Console.WriteLine($"Manufacturer is not found.");
            }

            try
            {
                var manufacturer = service.GetById(3);
                Console.WriteLine($"Manufacturer's name {manufacturer.Name}");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            var foundManufacturers = service.Search("China");

            Console.WriteLine();
            Console.WriteLine("Manufacturers from China:");

            //foreach (var manufacturer in foundManufacturers)
            //{
            //    Console.WriteLine($"Name {manufacturer.Name}");
            //}

            foundManufacturers.ForEach(m => Console.WriteLine($"Name: {m.Name}"));

            Console.WriteLine();
        }

        public static void InitializeStores(InMemoryDatabase database, PhoneStoreManagerDbContext context)
        {
            var store1 = new Store
            {
                Id = 1,
                Name = "SamsungStore",
                Address = "Belarus,Sovetskaya,Minsk"
            };
            var store2 = new Store
            {
                Id = 2,
                Name = "AppleStore",
                Address = "Belarus,Nyamigha,Brest"
            };
            var store3 = new Store
            {
                Id = 3,
                Name = "SamsungStore",
                Address = "Belarus,Hlebki,Minsk"
            };

            //var customrepository = new CustomStoreRepository(database);
            //var linqRepository = new LinqStoreRepository(database);
            //var adoRepository = new AdoStoreRepository();
            var efRepository = new EFStoreRepository(context);
            var service = new StoreService(efRepository);

            //service.Add(store1);
            //service.Add(store2);
            //service.Add(store3);

            var storesInMinsk = service.Search("minsk");
            Console.WriteLine("Stores in Minsk");

            storesInMinsk.ForEach(m => { Console.WriteLine($"{m.Name}"); });
            Console.WriteLine();


            //var store = repository.GetByName("Applestore");

            //  Console.WriteLine($"Name: {store.Name}");

        }

        public static void InitializePhones(InMemoryDatabase database, PhoneStoreManagerDbContext context)
        {
            var phone1 = new Phone
            {
                Id = 1,
                ManufacturerId = 1,
                Name = "Samsung S9"
            };

            var phone2 = new Phone
            {
                Id = 2,
                ManufacturerId = 2,
                Name = "Apple IPhone 10",
            };

            var phone3 = new Phone
            {
                Id = 3,
                ManufacturerId = 3,
                Name = "Xiaomi Mi Mix 3",
            };
            var phone4 = new Phone
            {
                ManufacturerId = 4,
                Name = "Samsung S7",
            };

            //var repository = new CustomPhoneRepository(database);
            //var service = new PhoneService(repository);
            //var adoRepository = new AdoPhoneRepository();
            var efRepository = new EFPhoneRepository(context);
            var service = new PhoneService(efRepository);
            //service.Add(phone1);
            //service.Add(phone2);
            //service.Add(phone3);
            //service.Add(phone4);
           
            var samsungS9 = service.GetById(3);
            //Console.WriteLine($"Address: {samsungS9.Manufacturer.Address}");
            //var foundPhones = service.Search("a");
            //foundPhones.ForEach(n => Console.WriteLine($"Phone: {n.Name}"));
        }
    }
}