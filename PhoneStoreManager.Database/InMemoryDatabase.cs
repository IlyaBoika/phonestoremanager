﻿using PhoneStoreManager.Domain;
using System.Collections.Generic;

namespace PhoneStoreManager.Database
{
    public class InMemoryDatabase
    {
        public List<Manufacturer> Manufacturers { get; set; }

        public List<Phone> Phones { get; set; }

        public List<Store> Stores { get; set; }

        //public List<StorePhones> StoresPhones{ get; set; }


        public InMemoryDatabase()
        {
            Manufacturers = new List<Manufacturer>();
            Phones = new List<Phone>();
            Stores = new List<Store>();
            //StoresPhones = new List<StorePhones>();
        }
    }
}
