﻿using System.Collections.Generic;

namespace PhoneStoreManager.Domain
{
    public class Manufacturer
    {
        public int Id { get; set; }

        public string Name { get; set; }
        public string Address { get; set; }

        public string SearchField { get; set; }

        #region Navigation Properties

        public List<Phone> Phones { get; set; }
    
        #endregion
    }
}