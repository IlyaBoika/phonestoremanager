﻿using System.Collections.Generic;

namespace PhoneStoreManager.Domain
{
    public class Phone
    {
        public int Id { get; set; }

        public Manufacturer Manufacturer { get; set; }

        public string Name { get; set; }

        public int ManufacturerId { get; set; }

        public string SearchField { get; set; }

        public string Description { get; set; }

        public decimal ScreenSize { get; set; }

        public int OSId { get; set; }

        public PhoneOS PhoneOS { get; set; }

        public List<StorePhone> StorePhones { get; set; }

    }
}
