﻿using System.Collections.Generic;

namespace PhoneStoreManager.Domain
{
    public class PhoneColor
    {
        public int Id { get; set; }

        public int PhoneId { get; set; }

        public string Color { get; set; }

        public string Code { get; set; }

        public List<StorePhone> StorePhones { get; set; }
    }
}
