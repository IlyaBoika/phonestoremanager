﻿using System.Collections.Generic;

namespace PhoneStoreManager.Domain
{
    public class PhoneOS
    {
        public int Id { get; set; }

        public string OS { get; set; }

        public List<Phone> Phones { get; set; }
    }
}
