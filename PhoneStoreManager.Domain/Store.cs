﻿using System.Collections.Generic;

namespace PhoneStoreManager.Domain
{
    public class Store
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string SearchField { get; set; }

        public List<StorePhone> StorePhones { get; set; }
    }
}
