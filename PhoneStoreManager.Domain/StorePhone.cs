﻿namespace PhoneStoreManager.Domain
{
    public class StorePhone
    {
        public int StoreId { get; set; }
        public int PhoneId { get; set; }
        public decimal Price { get; set; }
        public int Amount { get; set; }
        public int ColorId { get; set; }

        public Store Store { get; set; }
        public Phone Phone { get; set; }
        public PhoneColor PhoneColor { get; set; }
    }
}