﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using PhoneStoreManager.Domain;

namespace PhoneStoreManager.EntityFramework.Mapping
{
    public class ManufacturerMap : IEntityTypeConfiguration<Manufacturer>
    {
        public void Configure(EntityTypeBuilder<Manufacturer> builder)
        {
            builder.ToTable("Manufacturers");

            builder.HasKey(_ => _.Id);

            builder.Property(_ => _.Id).HasColumnName("Id").ValueGeneratedOnAdd();
            builder.Property(_ => _.Name).HasColumnName("Name").HasMaxLength(100).IsRequired();
            builder.Property(_ => _.Address).HasColumnName("Address").HasMaxLength(100).IsRequired();
            builder.Property(_ => _.SearchField).HasColumnName("SearchField").HasMaxLength(255);

            builder.HasMany(m => m.Phones)
                   .WithOne(p => p.Manufacturer)
                   .HasForeignKey(p => p.ManufacturerId);
        }
    }
}