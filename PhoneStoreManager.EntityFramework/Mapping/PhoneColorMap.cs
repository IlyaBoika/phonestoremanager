﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using PhoneStoreManager.Domain;

namespace PhoneStoreManager.EntityFramework
{
    internal class PhoneColorMap : IEntityTypeConfiguration<PhoneColor>
    {
        public void Configure(EntityTypeBuilder<PhoneColor> builder)
        {
            builder.ToTable("PhoneColor");

            builder.HasKey(_ => _.Id);

            builder.Property(_ => _.Id).HasColumnName("Id").ValueGeneratedOnAdd();
            builder.Property(_ => _.PhoneId).HasColumnName("PhoneId").IsRequired();
            builder.Property(_ => _.Color).HasColumnName("Color").HasMaxLength(50).IsRequired();
            builder.Property(_ => _.Code).HasColumnName("Code").HasMaxLength(50).IsRequired();

            builder.HasMany(c => c.StorePhones)
                   .WithOne(sp => sp.PhoneColor)
                   .HasForeignKey(sp => sp.ColorId);
        }
    }
}