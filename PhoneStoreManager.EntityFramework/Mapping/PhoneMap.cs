﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using PhoneStoreManager.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace PhoneStoreManager.EntityFramework.Mapping
{
    public class PhoneMap : IEntityTypeConfiguration<Phone>
    {
        public void Configure(EntityTypeBuilder<Phone> builder)
        {
            builder.ToTable("Phones");

            builder.HasKey(_ => _.Id);

            builder.Property(_ => _.Id).HasColumnName("Id").ValueGeneratedOnAdd();
            builder.Property(_ => _.Name).HasColumnName("Name").HasMaxLength(100).IsRequired();
            builder.Property(_ => _.ManufacturerId).HasColumnName("ManufacturerId").IsRequired();
            builder.Property(_ => _.SearchField).HasColumnName("SearchField").HasMaxLength(255);
            builder.Property(_ => _.Description).HasColumnName("Description").IsRequired();
            builder.Property(_ => _.ScreenSize).HasColumnName("ScreenSize").IsRequired();
            builder.Property(_ => _.OSId).HasColumnName("OSId").IsRequired();

            //builder.Ignore(_ => _.Manufacturer);
        }
    }
}
