﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using PhoneStoreManager.Domain;

namespace PhoneStoreManager.EntityFramework.Mapping
{
    public class PhoneOSMap : IEntityTypeConfiguration<PhoneOS>
    {
        public void Configure(EntityTypeBuilder<PhoneOS> builder)
        {
            builder.ToTable("PhoneOS");

            builder.HasKey(_ => _.Id);

            builder.Property(_ => _.Id).HasColumnName("Id").ValueGeneratedOnAdd();
            builder.Property(_ => _.OS).HasColumnName("OS").HasMaxLength(50).IsRequired();

            builder.HasMany(o => o.Phones)
                   .WithOne(p => p.PhoneOS)
                   .HasForeignKey(p => p.OSId);
        }
    }
}
