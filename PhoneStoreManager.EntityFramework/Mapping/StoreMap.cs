﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using PhoneStoreManager.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace PhoneStoreManager.EntityFramework.Mapping
{
    public class StoreMap: IEntityTypeConfiguration<Store>
    {
        public void Configure(EntityTypeBuilder<Store> builder)
      {
            builder.ToTable("Stores");

            builder.HasKey(_ => _.Id);

            builder.Property(_ => _.Id).HasColumnName("Id").ValueGeneratedOnAdd();
            builder.Property(_ => _.Name).HasColumnName("Name").HasMaxLength(100).IsRequired();
            builder.Property(_ => _.Address).HasColumnName("Address").HasMaxLength(100).IsRequired();
            builder.Property(_ => _.SearchField).HasColumnName("SearchField").HasMaxLength(255);
        }
    }
}
