﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using PhoneStoreManager.Domain;

namespace PhoneStoreManager.EntityFramework.Mapping
{
    public class StorePhonesMap : IEntityTypeConfiguration<StorePhone>
    {
        public void Configure(EntityTypeBuilder<StorePhone> builder)
        {
            builder.ToTable("StorePhones");

            builder.Property(_ => _.StoreId).HasColumnName("StoreId").IsRequired();
            builder.Property(_ => _.PhoneId).HasColumnName("PhoneId").IsRequired();
            builder.Property(_ => _.Price).HasColumnName("Price").HasColumnType("decimal(18, 2)").IsRequired();
            builder.Property(_ => _.Amount).HasColumnName("Amount").IsRequired();
            builder.Property(_ => _.ColorId).HasColumnName("ColorId").IsRequired();

            builder.HasKey(sp => new { sp.StoreId, sp.PhoneId, sp.ColorId });

            builder.HasOne(sp => sp.Store)
                   .WithMany(s => s.StorePhones)
                   .HasForeignKey(sp => sp.StoreId);

            builder.HasOne(sp => sp.Phone)
                   .WithMany(p => p.StorePhones)
                   .HasForeignKey(sp => sp.PhoneId);
        }
    }
}
