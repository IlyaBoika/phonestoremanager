﻿using Microsoft.EntityFrameworkCore;
using PhoneStoreManager.Domain;
using PhoneStoreManager.EntityFramework.Mapping;

namespace PhoneStoreManager.EntityFramework
{
    public class PhoneStoreManagerDbContext: DbContext
    {
        public DbSet<Manufacturer> Manufacturers { get; protected set; }

        public DbSet<Phone> Phones { get; protected set; }

        public DbSet<Store> Stores { get; protected set; }

        public DbSet<StorePhone> StorePhones { get; protected set; }

        public DbSet<PhoneOS> PhoneOSes { get; protected set; }

        public DbSet<PhoneColor> PhoneColors { get; protected set; }

        public PhoneStoreManagerDbContext(DbContextOptions options)
            : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new ManufacturerMap());

            modelBuilder.ApplyConfiguration(new PhoneMap());

            modelBuilder.ApplyConfiguration(new StoreMap());

            modelBuilder.ApplyConfiguration(new StorePhonesMap());

            modelBuilder.ApplyConfiguration(new PhoneOSMap());

            modelBuilder.ApplyConfiguration(new PhoneColorMap());
        }
    }
}