﻿using PhoneStoreManager.Domain;
using PhoneStoreManager.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;

namespace PhoneStoreManager.Repositories
{
    public class AdoManufacturerRepository 
    {
        private string _connectionString;

        public AdoManufacturerRepository()
        {
            _connectionString = Constants.ConnectionString;
        }

        public void Add(Manufacturer manufacturer)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                var queryString = "INSERT INTO Manufacturers(Name, Address, SearchField) VALUES(@name, @address, @searchField)";

                SqlCommand command = new SqlCommand(queryString, connection);
                command.Parameters.AddWithValue("@name", manufacturer.Name);
                command.Parameters.AddWithValue("@address", manufacturer.Address);
                command.Parameters.AddWithValue("@searchField", manufacturer.SearchField);

                connection.Open();

                command.ExecuteNonQuery();
            }
        }

        public Manufacturer FindByName(string name)
        {
            Manufacturer manufacturer = null;//????????

            using (var connection = new SqlConnection(_connectionString))
            {
                var queryString = "SELECT Id, Name, Address, SearchField FROM Manufacturers WHERE Name = @name";

                SqlCommand command = new SqlCommand(queryString, connection);
                command.Parameters.AddWithValue("@name", name);

                connection.Open();

                SqlDataReader reader = command.ExecuteReader();

                if (reader.Read())
                {
                   

                    manufacturer = CreateManufacturer(reader);
                }
                else
                {
                    return null;
                }

                reader.Close();
            }

            return manufacturer;
        }

        public Manufacturer GetById(int id)
        {
            Manufacturer manufacturer = null;

            using (var connection = new SqlConnection(_connectionString))
            {
                var queryString = "SELECT Id, Name, Address, SearchField FROM Manufacturers WHERE Id = @id";

                SqlCommand command = new SqlCommand(queryString, connection);
                command.Parameters.AddWithValue("@id", id);

                connection.Open();

                SqlDataReader reader = command.ExecuteReader();

                if (reader.Read())
                {
                    manufacturer = CreateManufacturer(reader);
                }
                else
                {
                    throw new ArgumentException($"Manufacturer is not found by id {id}");
                }

                reader.Close();
            }

            return manufacturer;
        }

        public List<Manufacturer> Search(string searchText)
        {
            var manufacturers = new List<Manufacturer>();

            using (var connection = new SqlConnection(_connectionString))
            {
                var queryString = @"SELECT Id, Name, Address, SearchField FROM Manufacturers WHERE SearchField LIKE @searchText";

                SqlCommand command = new SqlCommand(queryString, connection);
                command.Parameters.AddWithValue("@searchText", "%" + searchText + "%");

                connection.Open();

                SqlDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    var manufacturer = CreateManufacturer(reader);
                    manufacturers.Add(manufacturer);
                }

                reader.Close();
            }

            return manufacturers;
        }

        private Manufacturer CreateManufacturer(SqlDataReader reader)
        {
            return new Manufacturer
            {
                Id = Convert.ToInt32(reader["Id"]),
                Name = Convert.ToString(reader["Name"]),
                Address = Convert.ToString(reader["Address"]),
                SearchField = Convert.ToString(reader["SearchField"])
            };
        }
    }
}
