﻿using PhoneStoreManager.Domain;
using PhoneStoreManager.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;

namespace PhoneStoreManager.Repositories
{
    public class AdoPhoneRepository 
    {
        private string _connectionString;
        
        public AdoPhoneRepository()
        {
            _connectionString = Constants.ConnectionString;
        }
        public void Add(Phone phone)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                var queryString = "INSERT INTO Phones(Name, ManufacturerId, SearchField) VALUES(@name, @ManufacturerId, @searchField)";

                SqlCommand command = new SqlCommand(queryString, connection);
                command.Parameters.AddWithValue("@name", phone.Name);
                command.Parameters.AddWithValue("@ManufacturerId", phone.ManufacturerId);
                command.Parameters.AddWithValue("@searchField", phone.SearchField);

                connection.Open();

                command.ExecuteNonQuery();
            }
        }

        public List<Phone> GetAllPhones()
        {
            throw new NotImplementedException();
        }

        public Phone GetById(int Id)
        {
            Phone phone = null;
            using (var connection = new SqlConnection(_connectionString))
            {
                var queryString = @"SELECT 
                                     p.Id AS PhoneId,
                                     p.Name AS PhoneName,
                                     p.ManufacturerId AS ManufacturerId,
                                     p.SearchField As PhoneSearchField,
                                     m.Name AS ManufacturerName,
                                     m.Address AS ManufacturerAddress,
                                     m.SearchField AS ManufacturerSearchField
                                   FROM Phones AS p
                                   JOIN Manufacturers AS m
                                   ON p.ManufacturerId = m.Id
                                   WHERE p.Id = @id";

                SqlCommand command = new SqlCommand(queryString, connection);
                command.Parameters.AddWithValue("@Id", Id);

                connection.Open();

                SqlDataReader reader = command.ExecuteReader();

                if (reader.Read())
                {
                    phone = CreatePhone(reader);

                    phone.Manufacturer = new Manufacturer();
                    phone.Manufacturer.Id = Convert.ToInt32(reader["ManufacturerId"]);
                    phone.Manufacturer.Name = Convert.ToString(reader["ManufacturerName"]);
                    phone.Manufacturer.Address = Convert.ToString(reader["ManufacturerAddress"]);
                    phone.Manufacturer.SearchField = Convert.ToString(reader["ManufacturerSearchField"]);
                }
                else
                {
                    throw new Exception($"Store is not found by Id{Id}");
                }

                reader.Close();
            }
            
            //   phone.Manufacturer.GetById(ManufacturerId) kak primenit etot metod?
            return phone;

        }

        public void Remove(int id)
        {
            throw new NotImplementedException();
        }

        public void Save(Phone phone)
        {
            throw new NotImplementedException();
        }

        public List<Phone> Search(string searchText)
        {
            return new List<Phone>();
        }

        private Phone CreatePhone(SqlDataReader reader)
        {
            return new Phone
            {
                Id = Convert.ToInt32(reader["PhoneId"]),
                Name = Convert.ToString(reader["PhoneName"]),
                ManufacturerId = Convert.ToInt32(reader["ManufacturerId"]),
                SearchField = Convert.ToString(reader["PhoneSearchField"])
            };
        }
    }
}
