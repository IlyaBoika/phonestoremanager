﻿using PhoneStoreManager.Domain;
using PhoneStoreManager.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;

namespace PhoneStoreManager.Repositories
{
    public class AdoStoreRepository 
    {
        private string _connectionString;
        public AdoStoreRepository()
        {
            _connectionString = Constants.ConnectionString;
        }

        public void Save(Store store)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                var queryString = "INSERT INTO Stores(Name, Address, SearchField) VALUES(@name, @address, @searchField)";

                SqlCommand command = new SqlCommand(queryString, connection);
                command.Parameters.AddWithValue("@name", store.Name);
                command.Parameters.AddWithValue("@address", store.Address);
                command.Parameters.AddWithValue("@searchField", store.SearchField);

                connection.Open();

                command.ExecuteNonQuery();
            }
        }

        public void AddPhoneToStore(int storeId, int phoneId, decimal price)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                var queryString = "INSERT INTO StorePhones(StoreId, PhoneId, Price) VALUES(@StoreId, @PhoneId, @Price)";

                SqlCommand command = new SqlCommand(queryString, connection);
                command.Parameters.AddWithValue("@StoreId", storeId);
                command.Parameters.AddWithValue("@PhoneId", phoneId);
                command.Parameters.AddWithValue("@Price", price);

                connection.Open();

                command.ExecuteNonQuery();
            }
        }

        public Store FindByName(string name)
        {
            Store store = null;
            using (var connection = new SqlConnection(_connectionString))
            {
                var queryString = "SELECT Id, Name, Address, SearchField FROM Stores WHERE Name = @name";

                SqlCommand command = new SqlCommand(queryString, connection);
                command.Parameters.AddWithValue("@name", name);

                connection.Open();

                SqlDataReader reader = command.ExecuteReader();

                if (reader.Read())
                {
                    store = CreateStore(reader);
                    //store.Phones = GetStorePhones(store.Id);
                }
                else
                {
                    return null;
                }

                reader.Close();
            }

            return store;

        }

        public Store GetById(int id)
        {
            Store store = null;
            using (var connection = new SqlConnection(_connectionString))
            {
                var queryString = "SELECT Id, Name, Address, SearchField FROM Stores WHERE Id = @Id";

                SqlCommand command = new SqlCommand(queryString, connection);
                command.Parameters.AddWithValue("@name", id);

                connection.Open();

                SqlDataReader reader = command.ExecuteReader();

                if (reader.Read())
                {
                    store = CreateStore(reader);
                    //store.Phones = GetStorePhones(id);
                }
                else
                {
                    throw new Exception($"Store is not found by Id {id}");
                }

                reader.Close();
            }

            return store;
        }

        private List<Phone> GetStorePhones(int storeId)
        {
            var phonesInStores = new List<Phone>();
            using (var connection = new SqlConnection(_connectionString))
            {
                //var queryString = @"Select  * From Phones inner join StorePhones on ID = PhoneId WHERE StoreId = @StoreId";
                var queryString = @"Select 
                                        p.Id as PhoneId,
                                        p.Name as PhoneName,
                                        p.ManufacturerId AS ManufacturerId,
                                        p.SearchField As PhoneSearchField,
                                        sp.StoreId as IdStore,
                                        sp.PhoneId as IdPhone,
                                        sp.Price as PhonePrice 
                                    FROM Phones as p 
                                    INNER JOIN StorePhones as sp 
                                    ON p.Id = sp.PhoneId 
                                    WHERE sp.StoreId = @StoreId";

                SqlCommand command = new SqlCommand(queryString, connection);
                command.Parameters.AddWithValue("@StoreId", storeId);

                connection.Open();

                SqlDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    var phone = CreatePhone(reader);
                    phonesInStores.Add(phone);
                }

                reader.Close();
            }
            return phonesInStores;

        }

        public List<Store> Search(string searchText)
        {
            var stores = new List<Store>();

            using (var connection = new SqlConnection(_connectionString))
            {
                var queryString = @"SELECT Id, Name, Address, SearchField FROM Stores WHERE SearchField LIKE @searchText";

                SqlCommand command = new SqlCommand(queryString, connection);
                command.Parameters.AddWithValue("@searchText", "%"+searchText+"%");

                connection.Open();

                SqlDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    Store store = CreateStore(reader);
                    //store.Phones = GetStorePhones(store.Id);
                    stores.Add(store);
                }

                reader.Close();
            }

            return stores;
        }

        private Store CreateStore(SqlDataReader reader)
        {
            return new Store
            {
                Id = Convert.ToInt32(reader["Id"]),
                Name = Convert.ToString(reader["Name"]),
                Address = Convert.ToString(reader["Address"]),
                SearchField = Convert.ToString(reader["SearchField"])
            };
        }

        private Phone CreatePhone(SqlDataReader reader)
        {
            return new Phone
            {
                Id = Convert.ToInt32(reader["PhoneId"]),
                Name = Convert.ToString(reader["PhoneName"]),
                ManufacturerId = Convert.ToInt32(reader["ManufacturerId"]),
                SearchField = Convert.ToString(reader["PhoneSearchField"])
            };
        }

        public List<Store> GetAllStores()
        {
            throw new NotImplementedException();
        }

        public void Remove(int id)
        {
            throw new NotImplementedException();
        }
    }
}
