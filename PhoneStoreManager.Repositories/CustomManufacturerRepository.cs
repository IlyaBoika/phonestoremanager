﻿using PhoneStoreManager.Database;
using PhoneStoreManager.Domain;
using PhoneStoreManager.Repositories.Interfaces;
using System;
using System.Collections.Generic;

namespace PhoneStoreManager.Repositories
{
    public class CustomManufacturerRepository
    {
        private InMemoryDatabase _database;

        public CustomManufacturerRepository(InMemoryDatabase database)
        {
            _database = database;
        }

        public void Add(Manufacturer manufacturer)
        {
            _database.Manufacturers.Add(manufacturer);
        }

        public Manufacturer GetById(int id)
        {
            var manufacturers = _database.Manufacturers;
            for (int i = 0; i < manufacturers.Count; i++)
            {
                if (manufacturers[i].Id == id)
                {
                    return manufacturers[i];
                }
            }

            return null;
        }

        public Manufacturer FindByName(string name)
        {
            var manufacturers = this._database.Manufacturers;
            var nameInLowerCase = name.ToLower();

            for (int i = 0; i < manufacturers.Count; i++)
            {
                if (manufacturers[i].Name.ToLower() == nameInLowerCase)
                {
                    return manufacturers[i];
                }
            }

            return null;
        }

        public List<Manufacturer> Search(string searchText)
        {
            var result = new List<Manufacturer>();
            var manufacturers = _database.Manufacturers;
            var searchTextInUpperCase = searchText.ToUpper();

            for (int i = 0; i < manufacturers.Count; i++)
            {
                if (manufacturers[i].SearchField.Contains(searchTextInUpperCase))
                {
                    result.Add(manufacturers[i]);
                }
            }

            return result;
        }
    }
}