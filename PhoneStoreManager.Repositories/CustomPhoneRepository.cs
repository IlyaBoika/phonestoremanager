﻿using PhoneStoreManager.Database;
using PhoneStoreManager.Domain;
using PhoneStoreManager.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace PhoneStoreManager.Repositories
{
    public class CustomPhoneRepository
    {
        private InMemoryDatabase _database;


        public CustomPhoneRepository(InMemoryDatabase database)
        {
            _database = database;

        }

        public void Add(Phone phone)
        {
            _database.Phones.Add(phone);
        }

        public List<Phone> GetAllPhones()
        {
            throw new NotImplementedException();
        }

        public Phone GetById(int Id)
        {
            var phones = _database.Phones;

            for (int i = 0; i < phones.Count; i++)
            {
                if (phones[i].Id == Id)
                {

                    //var manufacturers = _database.Manufacturers;

                    //foreach (var manufacturer in manufacturers)
                    //{
                    //    if (manufacturer.Id == phones[i].ManufacturerId)
                    //    {
                    //        phones[i].Manufacturer = manufacturer;
                    //        break;
                    //    }
                    //}

                    phones[i].Manufacturer = GetManufacturerById(phones[i].ManufacturerId);

                    return phones[i];
                }
            }

            return null;
        }

        public void Remove(int id)
        {
            throw new NotImplementedException();
        }

        public void Save(Phone phone)
        {
            throw new NotImplementedException();
        }

        public List<Phone> Search(string searchText)
        {
            var result = new List<Phone>();
            var phones = _database.Phones;
            var searchTextInUpperCase = searchText.ToUpper();

            for (int i = 0; i < phones.Count; i++)
            {
                if (phones[i].SearchField.Contains(searchTextInUpperCase))
                {
                    result.Add(phones[i]);
                }
            }

            return result;
        }

        private Manufacturer GetManufacturerById(int manufacturerId)
        {
            var manufacturers = _database.Manufacturers;

            for (int i = 0; i < manufacturers.Count; i++)
            {
                if (manufacturers[i].Id == manufacturerId)
                {
                    return manufacturers[i];
                }

            }

            return null;
        }
    }
}
