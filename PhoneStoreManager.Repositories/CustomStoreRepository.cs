﻿using PhoneStoreManager.Database;
using PhoneStoreManager.Domain;
using PhoneStoreManager.Repositories.Interfaces;
using System.Collections.Generic;

namespace PhoneStoreManager.Repositories
{
    public class CustomStoreRepository
    {
        private InMemoryDatabase _database;
        
        public  CustomStoreRepository(InMemoryDatabase database)
        {
            _database = database;
        }

        public void Save(Store store)
        {
            _database.Stores.Add(store);
        }

        public Store GetById(int Id)
        {
            var stores = _database.Stores;

            for (int i = 0; i< stores.Count; i++)
            {
                if (Id == stores[i].Id)
                    return stores[i];

            }
            return null;
        }

        public Store FindByName(string name)
        {
            var stores = _database.Stores;
            var nameInLowerCase = name.ToLower();

            for (int i = 0; i < stores.Count; i++)
            {
                if (stores[i].Name.ToLower() == nameInLowerCase)
                {
                    return stores[i];
                }
            }

            return null;
        }

        public List<Store> Search(string searchText)
        {
            var result = new List<Store>();
            var stores = _database.Stores;
            var searchTextInUpperCase = searchText.ToUpper();

            for (int i=0; i < stores.Count; i++)
            {
                if (stores[i].SearchField.Contains(searchTextInUpperCase))
                {
                    result.Add(stores[i]);
                }

            }
            return result;
        }

        public void AddPhoneToStore(int StoreId, int PhoneId, decimal Price)
        {
            throw new System.NotImplementedException();
        }

        public List<Phone> GetStorePhones(int StoreId)
        {
            throw new System.NotImplementedException();
        }

        public List<Store> GetAllStores()
        {
            throw new System.NotImplementedException();
        }

        public void Remove(int id)
        {
            throw new System.NotImplementedException();
        }
    }
}