﻿using Microsoft.EntityFrameworkCore;
using PhoneStoreManager.Domain;
using PhoneStoreManager.EntityFramework;
using PhoneStoreManager.Repositories.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace PhoneStoreManager.Repositories
{
    public class EFManufacturerRepository : IManufacturerRepository
    {
        private readonly PhoneStoreManagerDbContext _context;

        public EFManufacturerRepository(PhoneStoreManagerDbContext context)
        {
            _context = context;
        }

        public void Add(Manufacturer manufacturer)
        {
            _context.Add(manufacturer);
            _context.SaveChanges();
        }

        public Manufacturer FindByName(string name)
        {
            return _context
                .Manufacturers
                .FirstOrDefault(_ => _.Name == name);
        }

        public List<Manufacturer> GetAllManufacturers()
        {
            return _context.Manufacturers.ToList();
        }

        public Manufacturer GetById(int id)
        {
            return _context.Manufacturers.First(_ => _.Id == id);
        }

        public void Remove(int id)
        {
            Manufacturer manufacturer = new Manufacturer { Id = id };
            _context.Entry(manufacturer).State = EntityState.Deleted;
            _context.SaveChanges();
        }

        public void Save(Manufacturer manufacturer)
        {
            if (manufacturer.Id == 0)
            {
                _context.Add(manufacturer);
            }
            else
            {
                _context.Attach(manufacturer);
                _context.Entry(manufacturer).State = EntityState.Modified;
            }
            _context.SaveChanges();
        }

        public List<Manufacturer> Search(string searchText)
        {
            return _context.Manufacturers.Where(_ => _.SearchField.Contains(searchText)).ToList();
        }
    }
}