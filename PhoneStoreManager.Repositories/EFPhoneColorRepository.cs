﻿using PhoneStoreManager.Domain;
using PhoneStoreManager.EntityFramework;
using PhoneStoreManager.Repositories.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace PhoneStoreManager.Repositories
{
    public class EFPhoneColorRepository : IPhoneColorRepository
    {
        private readonly PhoneStoreManagerDbContext _context;

        public EFPhoneColorRepository(PhoneStoreManagerDbContext context)
        {
            _context = context;
        }

        public List<PhoneColor> GetPhoneColors(int id)
        {
            return _context.PhoneColors.Where(_ => _.PhoneId == id).ToList();
        }
    }
}
