﻿using PhoneStoreManager.Domain;
using PhoneStoreManager.EntityFramework;
using PhoneStoreManager.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PhoneStoreManager.Repositories
{
    public class EFPhoneOSRepository : IPhoneOSRepository
    {

        private readonly PhoneStoreManagerDbContext _context;

        public EFPhoneOSRepository(PhoneStoreManagerDbContext context)
        {
            _context = context;
        }

        public List<PhoneOS> GetAllPhoneOS()
        {
            return _context.PhoneOSes.ToList();
        }
    }




}
