﻿using Microsoft.EntityFrameworkCore;
using PhoneStoreManager.Domain;
using PhoneStoreManager.EntityFramework;
using PhoneStoreManager.Repositories.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace PhoneStoreManager.Repositories
{
    public class EFPhoneRepository : IPhoneRepository
    {
        private readonly PhoneStoreManagerDbContext _context;

        public EFPhoneRepository(PhoneStoreManagerDbContext context)
        {
            _context = context;
        }

        public void Add(Phone phone)
        {
            _context.Add(phone);
            _context.SaveChanges();
        }

        public List<Phone> GetAllPhones()
        {
            return _context.Phones.Include(_=>_.PhoneOS).ToList();
        }

        public Phone GetById(int Id)
        {
            return _context
                    .Phones
                    .Include(_=>_.PhoneOS)
                    .First(_ => _.Id == Id);
        }

        public void Remove(int id)
        {
            Phone phone = new Phone { Id = id };
            _context.Entry(phone).State = EntityState.Deleted;
            _context.SaveChanges();
        }

        public void Save(Phone phone)
        {
            if (phone.Id == 0)
            {
                _context.Add(phone);
            }
            else
            {
                _context.Attach(phone);
                _context.Entry(phone).State = EntityState.Modified;
            }
            _context.SaveChanges();
        }

        public List<Phone> Search(string searchText)
        {
            return _context.
                     Phones.
                     Where(_ => _.SearchField.Contains(searchText)).
                     Include(_ => _.PhoneOS).
                     ToList();
        }
    }
}
