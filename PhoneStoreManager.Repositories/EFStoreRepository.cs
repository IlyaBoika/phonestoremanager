﻿using Microsoft.EntityFrameworkCore;
using PhoneStoreManager.Domain;
using PhoneStoreManager.EntityFramework;
using PhoneStoreManager.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PhoneStoreManager.Repositories
{
    public class EFStoreRepository : IStoreRepository
    {
        private readonly PhoneStoreManagerDbContext _context;

        public EFStoreRepository(PhoneStoreManagerDbContext context)
        {
            _context = context;
        }

        public void Save(Store store)
        {
            if (store.Id == 0)
            {
                _context.Add(store);
            }
            else
            {
                _context.Attach(store);
                _context.Entry(store).State = EntityState.Modified;
            }

            _context.SaveChanges();
        }

        public List<Store> GetAllStores()
        {
            return _context.Stores.ToList();
        }

        public void AddPhoneToStore(int storeId, int phoneId, decimal price,int amount,int colorId)
        {
            var storePhones = new StorePhone
            {
                StoreId = storeId,
                PhoneId = phoneId,
                Price = price,
                Amount=amount,
                ColorId=colorId
            };

            _context.StorePhones.Add(storePhones);
            _context.SaveChanges();
        }

        public Store FindByName(string name)
        {
            return _context.Stores
                           .FirstOrDefault(_ => _.Name == name);
        }

        public Store GetById(int id)
        {
            return _context.Stores
                           .Include(_ => _.StorePhones)
                            .ThenInclude(_ => _.PhoneColor)
                           .First(_ => _.Id == id);
        }

        public List<Store> GetStoreWithPhonesById(int id)
        {
            throw new NotImplementedException();
        }

        public List<Store> Search(string searchText)
        {
            return _context.Stores.Where(_ => _.SearchField.Contains(searchText.ToUpper())).ToList();
        }

        public void Remove(int id)
        {
            var store = new Store{ Id = id };
            _context.Entry(store).State = EntityState.Deleted;
            _context.SaveChanges();
        }

        public void RemovePhonesFromStore(int phoneId, int storeId)
        {
            var storePhones = new StorePhone
            {
                StoreId = storeId,
                PhoneId = phoneId,             
            };

            _context.Entry(storePhones).State = EntityState.Deleted;
            _context.SaveChanges();
        }
    }
}
