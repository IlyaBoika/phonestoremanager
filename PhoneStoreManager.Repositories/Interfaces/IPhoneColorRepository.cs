﻿using PhoneStoreManager.Domain;
using System.Collections.Generic;

namespace PhoneStoreManager.Repositories.Interfaces
{
    public interface IPhoneColorRepository
    {
        List <PhoneColor> GetPhoneColors(int id);
    }
}
