﻿using PhoneStoreManager.Domain;
using System.Collections.Generic;

namespace PhoneStoreManager.Repositories.Interfaces
{
    public interface IPhoneOSRepository
    {
        List<PhoneOS> GetAllPhoneOS();
    }
}
