﻿using PhoneStoreManager.Domain;
using System.Collections.Generic;

namespace PhoneStoreManager.Repositories.Interfaces
{
    public interface IPhoneRepository
    {
        
        Phone GetById(int Id);
        void Add(Phone phone);
        List<Phone> Search(string searchText);
        List<Phone> GetAllPhones();
        void Save(Phone phone);
        void Remove(int id);
    }
}
