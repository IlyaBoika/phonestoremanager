﻿using PhoneStoreManager.Domain;
using System.Collections.Generic;

namespace PhoneStoreManager.Repositories.Interfaces
{
    public interface IStoreRepository
    {
        void Save(Store store);
        List<Store> GetAllStores();
        Store GetById(int Id);
        Store FindByName(string name);
        List<Store> Search(string searchText);
        void AddPhoneToStore(int storeId, int phoneId, decimal price,int amount,int colorId);
        void Remove(int id);
        void RemovePhonesFromStore(int phoneId, int storeId);
    }
}