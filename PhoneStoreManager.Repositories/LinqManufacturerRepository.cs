﻿using PhoneStoreManager.Database;
using PhoneStoreManager.Domain;
using PhoneStoreManager.Repositories.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace PhoneStoreManager.Repositories
{
    public class LinqManufacturerRepository
    {
        private InMemoryDatabase _database;

        public LinqManufacturerRepository(InMemoryDatabase database)
        {
            _database = database;
        }

        public void Add(Manufacturer manufacturer)
        {
            _database.Manufacturers.Add(manufacturer);
        }

        public Manufacturer GetById(int id)
        {
            return _database.Manufacturers.First(m => m.Id == id);
        }

        public Manufacturer FindByName(string name)
        {
            return _database.Manufacturers.FirstOrDefault(m => m.Name == name);
        }

        public List<Manufacturer> Search(string searchText)
        {
            var searchTextInUpperCase = searchText.ToUpper();
            return _database.Manufacturers.Where(m => m.SearchField.Contains(searchTextInUpperCase)).ToList();
        }
    }
}
