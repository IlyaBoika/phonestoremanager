﻿using PhoneStoreManager.Database;
using PhoneStoreManager.Domain;
using PhoneStoreManager.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PhoneStoreManager.Repositories
{
   public class LinqStoreRepository
    {
        private InMemoryDatabase _database;

        public LinqStoreRepository (InMemoryDatabase database)
        {
            _database = database;
        }
        public void Save(Store store)
        {
            _database.Stores.Add(store);
        }
        public Store GetById(int id)
        {
            return _database.Stores.First(m => m.Id == id);
        }
        public Store FindByName(string name)
        {
            return _database.Stores.FirstOrDefault(k => k.Name == name);
        }
        public List<Store> Search(string SearchText)
        {
            var searchTextInUpper = SearchText.ToUpper();
            return _database.Stores.Where(m => m.SearchField.Contains(searchTextInUpper)).ToList();
        }

        public void AddPhoneToStore(int StoreId, int PhoneId, decimal Price)
        {
            throw new NotImplementedException();
        }

        public List<Phone> GetStorePhones(int StoreId)
        {
            throw new NotImplementedException();
        }

        public List<Store> GetAllStores()
        {
            throw new NotImplementedException();
        }

        public void Remove(int id)
        {
            throw new NotImplementedException();
        }
    }
}
