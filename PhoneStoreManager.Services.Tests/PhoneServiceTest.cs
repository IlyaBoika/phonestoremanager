﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PhoneStoreManager.Domain;
using PhoneStoreManager.Repositories.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace PhoneStoreManager.Services.Tests
{
    [TestClass]
    public class PhoneServiceTest
    {
        private static readonly int Id = 1;

        private static readonly string ManufacturerName = "ManufacturerName";
        private static readonly string Address = "Address";
        private static readonly string ManufacturerSearchField = "ManufacturerSearchField";

        private static readonly string Name = "Name";
        private static readonly int ManufacturerId = 2;
        private static readonly string Description = "Description";
        private static readonly decimal ScreenSize = 6.03m;
        private static readonly int OSId = 1;

        #region Add

        [TestMethod]
        public void Add_General()
        {
            //Arrange
            var phoneRepositoryMock = new Mock<IPhoneRepository>();
            var service = new PhoneService(phoneRepositoryMock.Object);

            var phone = new Phone
            {
                Id = Id,
                Name = Name,
                ManufacturerId = ManufacturerId,
                Description = Description,
                ScreenSize = ScreenSize,
                OSId = OSId,
            };

            var searchField = GetSearchField();

            //Act
            service.Add(phone);

            //Assert
            phoneRepositoryMock.Verify(_ => _.Add(It.Is<Phone>(p => p.Id == Id
                                                                && p.Name == Name
                                                                && p.ManufacturerId == ManufacturerId
                                                                && p.Description == Description
                                                                && p.ScreenSize == ScreenSize
                                                                && p.OSId == OSId
                                                                && p.SearchField == searchField)), Times.Once);

        }

        #endregion

        #region GetAllPhones
        [TestMethod]
        public void GetAllPhones_General()
        {
            //Arrange
            var phoneRepositoryMock = new Mock<IPhoneRepository>();
            var service = new PhoneService(phoneRepositoryMock.Object);

            var searchField = GetSearchField();

            var expectedPhones = new List<Phone>
            {
                new Phone
                {
                    Id = Id,
                    Name = Name,
                    ManufacturerId = ManufacturerId,
                    Description = Description,
                    ScreenSize = ScreenSize,
                    OSId = OSId,
                    SearchField = searchField,
                },
                new Phone
                {
                    Id=2,
                }
            };

            phoneRepositoryMock.Setup(_ => _.GetAllPhones()).Returns(expectedPhones);

            //Act
            var actualphones = service.GetAllPhones();

            //Assert
            phoneRepositoryMock.Verify(_ => _.GetAllPhones(), Times.Once);

            var firstPhone = actualphones.First();

            Assert.AreEqual(Id, firstPhone.Id);
            Assert.AreEqual(Name, firstPhone.Name);
            Assert.AreEqual(ManufacturerId, firstPhone.ManufacturerId);
            Assert.AreEqual(Description, firstPhone.Description);
            Assert.AreEqual(ScreenSize, firstPhone.ScreenSize);
            Assert.AreEqual(OSId, firstPhone.OSId);
            Assert.AreEqual(searchField, firstPhone.SearchField);

            Assert.IsNotNull(actualphones.ElementAt(1));
        }
        #endregion

        #region GetById
        [TestMethod]
        public void GetById_HasExpectedResult()
        {
            //Arrange
            var phoneRepositoryMock = new Mock<IPhoneRepository>();
            var service = new PhoneService(phoneRepositoryMock.Object);

            var searchField = GetSearchField();

            var expectedPhone = new Phone
            {
                Id = Id,
                Name = Name,
                ManufacturerId = ManufacturerId,
                Description = Description,
                ScreenSize = ScreenSize,
                OSId = OSId,
                SearchField = searchField,
            };

            phoneRepositoryMock.Setup(_ => _.GetById(Id)).Returns(expectedPhone);

            //Act
            var actualPhone = service.GetById(Id);

            //Assert
            phoneRepositoryMock.Verify(_ => _.GetById(It.Is<int>(i => i == Id)), Times.Once);

            Assert.AreEqual(Id, actualPhone.Id);
            Assert.AreEqual(Name, actualPhone.Name);
            Assert.AreEqual(ManufacturerId, actualPhone.ManufacturerId);
            Assert.AreEqual(Description, actualPhone.Description);
            Assert.AreEqual(ScreenSize, actualPhone.ScreenSize);
            Assert.AreEqual(OSId, actualPhone.OSId);
            Assert.AreEqual(searchField, actualPhone.SearchField);
        }
        #endregion

        #region Remove
        [TestMethod]
        public void Remove_General()
        {
            //Arrange
            var phoneRepositoryMock = new Mock<IPhoneRepository>();
            var service = new PhoneService(phoneRepositoryMock.Object);

            //Act
            service.Remove(Id);

            //Assert
            phoneRepositoryMock.Verify(_ => _.Remove(It.Is<int>(i => i == Id)), Times.Once);
        }
        #endregion

        #region Save
        [TestMethod]
        public void Save_HasExpectedResult()
        {
            //Arrange
            var phoneRepositoryMock = new Mock<IPhoneRepository>();
            var service = new PhoneService(phoneRepositoryMock.Object);

            var searchField = GetSearchField();

            var phone = new Phone
            {
                Id = Id,
                Name = Name,
                ManufacturerId = ManufacturerId,
                Description = Description,
                ScreenSize = ScreenSize,
                OSId = OSId,
                SearchField = searchField,
            };

            //Act
            service.Save(phone);

            //Assert
            phoneRepositoryMock.Verify(_ => _.Save(It.Is<Phone>(p => p.Id == Id
                                                              && p.Name == Name
                                                              && p.ManufacturerId == ManufacturerId
                                                              && p.Description == Description
                                                              && p.ScreenSize == ScreenSize
                                                              && p.OSId == OSId
                                                              && p.SearchField == searchField)), Times.Once);

        }
        #endregion

        #region Search
        [TestMethod]
        public void Search_HasExpectedResult()
        {
            //Arrange
            var phoneRepositoryMock = new Mock<IPhoneRepository>();
            var service = new PhoneService(phoneRepositoryMock.Object);

            string searchText = "SearchText";

            var searchField = GetSearchField();

            var expectedPhones = new List<Phone>
            {
                new Phone
                {
                    Id = Id,
                    Name = Name,
                    ManufacturerId = ManufacturerId,
                    Description = Description,
                    ScreenSize = ScreenSize,
                    OSId = OSId,
                    SearchField = searchField,
                },
                new Phone
                {
                    Id=2,
                },
            };

            phoneRepositoryMock.Setup(_ => _.Search(searchText)).Returns(expectedPhones);

            //Act
            var actualphones = service.Search(searchText);

            //Assert
            phoneRepositoryMock.Verify(_ => _.Search(It.Is<string>(s => s == searchText)), Times.Once);

            var firstPhone = actualphones.First();

            Assert.AreEqual(Id, firstPhone.Id);
            Assert.AreEqual(Name, firstPhone.Name);
            Assert.AreEqual(ManufacturerId, firstPhone.ManufacturerId);
            Assert.AreEqual(Description, firstPhone.Description);
            Assert.AreEqual(ScreenSize, firstPhone.ScreenSize);
            Assert.AreEqual(OSId, firstPhone.OSId);
            Assert.AreEqual(searchField, firstPhone.SearchField);

            Assert.IsNotNull(actualphones.ElementAt(1));
        }
        #endregion

        #region Helpers

        private Manufacturer BuildManufacturer()
        {
            return new Manufacturer
            {
                Id = ManufacturerId,
                Name = ManufacturerName,
                Address = Address,
                SearchField = ManufacturerSearchField
            };
        }

        private string GetSearchField()
        {
            return (Name + " " + Description).ToUpper();
        }
        #endregion

    }



}