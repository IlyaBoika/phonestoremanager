﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PhoneStoreManager.Domain;
using PhoneStoreManager.Repositories.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace PhoneStoreManager.Services.Tests
{
    [TestClass]
    public class StoreServiceTest
    {
        private static readonly int Id = 1;
        private static readonly string Name = "StoreName";
        private static readonly string Address = "Address";
        private static readonly string SearchText = "Store";
        private static readonly int storeId = 1;
        private static readonly int phoneId = 2;
        private static readonly decimal price = 100;
        private static readonly int amount = 5;
        private static readonly int colorId = 1;

        #region Save
        [TestMethod]
        public void Save_General()
        {
            //Arrange
            var store = new Store
            {
                Name = Name,
                Address = Address
            };

            var storeRepositoryMock = new Mock<IStoreRepository>();
            var service = new StoreService(storeRepositoryMock.Object);

            var searchField = GetSearchField();

            //Act
            service.Save(store);

            //Assert
            storeRepositoryMock.
                Verify(_ => _.Save(It.Is<Store>(s =>
                s.Address == Address
                && s.Name == Name
                && s.SearchField == searchField)),
                Times.Once);
        }
        #endregion

        #region GetAllStores
        [TestMethod]
        public void GetAllStores_HasExpectedResult()
        {
            //Arrange
            var storeRepositoryMock = new Mock<IStoreRepository>();
            var service = new StoreService(storeRepositoryMock.Object);

            var searchField = GetSearchField();

            var expectedStores = new List<Store>
            {
                new Store
                {
                    Id = Id,
                    Name = Name,
                    Address = Address,
                    SearchField = searchField
                },
                new Store
                {
                    Id = 2
                }
            };

            storeRepositoryMock.Setup(_ => _.GetAllStores())
                .Returns(expectedStores);

            //Act
            var actualStores = service.GetAllStores();

            //Assert
            storeRepositoryMock.Verify(_ => _.GetAllStores(), Times.Once);

            var firstStore = actualStores.First();

            Assert.AreEqual(Id, firstStore.Id);
            Assert.AreEqual(Name, firstStore.Name);
            Assert.AreEqual(Address, firstStore.Address);
            Assert.AreEqual(searchField, firstStore.SearchField);

            Assert.IsNotNull(actualStores.ElementAt(1));
        }
        #endregion

        #region GetById
        [TestMethod]
        public void GetById_HasExpectedResult()
        {
            //Arrange
            var storeRepositoryMock = new Mock<IStoreRepository>();
            var service = new StoreService(storeRepositoryMock.Object);

            string searchField = GetSearchField();

            var expectedStore = new Store
            {
                Id = Id,
                Name = Name,
                Address = Address,
                SearchField = searchField
            };

            storeRepositoryMock.Setup(_ => _.GetById(Id)).Returns(expectedStore);

            //Act
            var actualStore = service.GetById(Id);

            //Assert
            storeRepositoryMock.
               Verify(_ => _.GetById(It.Is<int>(i => i == Id)),
               Times.Once);

            Assert.AreEqual(Id, actualStore.Id);
            Assert.AreEqual(Name, actualStore.Name);
            Assert.AreEqual(Address, actualStore.Address);
            Assert.AreEqual(searchField, actualStore.SearchField);
        }
        #endregion

        #region FindByName
        [TestMethod]
        public void FindByName_HasExpectedResult()
        {
            //Arrange
            var storeRepositoryMock = new Mock<IStoreRepository>();
            var service = new StoreService(storeRepositoryMock.Object);

            string searchField = GetSearchField();

            var expectedStore = new Store
            {
                Id = Id,
                Name = Name,
                Address = Address,
                SearchField = searchField
            };

            storeRepositoryMock.Setup(_ => _.FindByName(Name)).Returns(expectedStore);

            //Act
            var actualStore = service.FindByName(Name);

            //Assert
            storeRepositoryMock.
              Verify(_ => _.FindByName(It.Is<string>(s => s == Name)),
              Times.Once);

            Assert.AreEqual(Id, actualStore.Id);
            Assert.AreEqual(Name, actualStore.Name);
            Assert.AreEqual(Address, actualStore.Address);
            Assert.AreEqual(searchField, actualStore.SearchField);

        }
        #endregion

        #region Search
        [TestMethod]
        public void Search_HasExpectedResult()
        {
            //Arrange
            var storeRepositoryMock = new Mock<IStoreRepository>();
            var service = new StoreService(storeRepositoryMock.Object);

            string searchField = GetSearchField();

            var expectedStores = new List<Store>
            {
                new Store
                {
                    Id = Id,
                    Name = Name,
                    Address = Address,
                    SearchField = searchField
                },
                new Store
                {
                    Id = 2,
                }

            };

            storeRepositoryMock.Setup(_ => _.Search(SearchText)).Returns(expectedStores);

            //Act
            var actualStores = service.Search(SearchText);

            //Assert
            storeRepositoryMock.Verify(_ => _.Search(It.Is<string>(s => s == SearchText)), Times.Once);

            var firstStore = actualStores.First();

            Assert.AreEqual(Id, firstStore.Id);
            Assert.AreEqual(Name, firstStore.Name);
            Assert.AreEqual(Address, firstStore.Address);
            Assert.AreEqual(searchField, firstStore.SearchField);

            Assert.IsNotNull(actualStores.ElementAt(1));
        }
        #endregion

        #region AddPhoneToStore
        [TestMethod]
        public void AddPhoneToStore_General()
        {
            //Arrange
            var storeRepositoryMock = new Mock<IStoreRepository>();
            var service = new StoreService(storeRepositoryMock.Object);

            //Act
            service.AddPhoneToStore(storeId, phoneId, price, amount, colorId);

            //Assert
            storeRepositoryMock.Verify(_ => _.AddPhoneToStore(It.Is<int>(a => a == storeId),
                                                              It.Is<int>(a => a == phoneId),
                                                              It.Is<decimal>(a => a == price),
                                                              It.Is<int>(a => a == amount),
                                                              It.Is<int>(a => a == colorId)), Times.Once);

        }
        #endregion

        #region Remove
        [TestMethod]
        public void Remove_General()
        {
            //Arrange
            var storeRepositoryMock = new Mock<IStoreRepository>();
            var service = new StoreService(storeRepositoryMock.Object);

            //Act
            service.Remove(Id);

            //Assert
            storeRepositoryMock.Verify(_ => _.Remove(It.Is<int>(i => i == Id)), Times.Once);
        }
        #endregion

        #region RemovePhonesFromStore
        [TestMethod]
        public void RemovePhonesFromStore_General()
        {
            //Arrange
            var storeRepositoryMock = new Mock<IStoreRepository>();
            var service = new StoreService(storeRepositoryMock.Object);

            //Act
            service.RemovePhonesFromStore(phoneId, storeId);

            //Assert
            storeRepositoryMock.Verify(_ => _.RemovePhonesFromStore(It.Is<int>(i => i == phoneId),
                                                                    It.Is<int>(i => i == storeId)), Times.Once);
        }
        #endregion

        #region Helpers
        private string GetSearchField()
        {
            return (Address + " " + Name).ToUpper();
        }
        #endregion
    }
}