﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace PhoneStoreManager.Services.Tests
{
    public class Calculator
    {
        public int Sum(int a, int b)
        {
            return a + b;
        }
    }

    [TestClass]
    public class UnitTest
    {
        [TestMethod]
        public void Calculator_Sum_General()
        {
            (int a, int b) = (2, 3);

            var calculator = new Calculator();
            var result = calculator.Sum(a, b);

            Assert.AreEqual(5, result);

            (a, b) = (4, 5);
            result = calculator.Sum(a, b);

            Assert.AreEqual(9, result);
        }
    }
}
