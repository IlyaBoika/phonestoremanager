﻿using PhoneStoreManager.Domain;
using System.Collections.Generic;

namespace PhoneStoreManager.Services.Interfaces
{
   public interface IManufacturerService
    {
        void Add(Manufacturer manufacturer);
        Manufacturer GetById(int id);
        Manufacturer FindByName(string name);
        List<Manufacturer> Search(string searchText);
        List<Manufacturer> GetAllManufacturers();
        void Save(Manufacturer manufacturer);
        void Remove(int id);
    }
}
