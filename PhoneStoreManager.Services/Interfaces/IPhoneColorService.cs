﻿using PhoneStoreManager.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace PhoneStoreManager.Services.Interfaces
{
    public interface IPhoneColorService
    {
        List<PhoneColor> GetPhoneColors(int id);
    }
}
