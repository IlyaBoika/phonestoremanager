﻿using PhoneStoreManager.Domain;
using System.Collections.Generic;

namespace PhoneStoreManager.Services.Interfaces
{
    public interface IPhoneOSService
    {
        List<PhoneOS> GetAllPhoneOS();

    }
}
