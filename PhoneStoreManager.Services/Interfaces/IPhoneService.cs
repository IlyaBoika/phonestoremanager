﻿using PhoneStoreManager.Domain;
using System.Collections.Generic;

namespace PhoneStoreManager.Services.Interfaces
{
    public interface IPhoneService
    {
        void Add(Phone phone);
        Phone GetById(int Id);
        List<Phone> Search(string Searchtext);
        List<Phone> GetAllPhones();
        void Save(Phone phone);
        void Remove(int id);
    }
}
