﻿using System.Collections.Generic;
using PhoneStoreManager.Domain;

namespace PhoneStoreManager.Services.Interfaces
{
    public interface IStoreService
    {
        void Save(Store store);
        List<Store> GetAllStores();
        void AddPhoneToStore(int storeId, int phoneId, decimal price,int amount,int colorId);
        Store FindByName(string name);
        Store GetById(int Id);
        void Remove(int Id);
        List<Store> Search(string searchText);
        void RemovePhonesFromStore(int phoneId, int storeId);
    }
}