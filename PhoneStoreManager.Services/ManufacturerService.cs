﻿using PhoneStoreManager.Domain;
using PhoneStoreManager.Repositories.Interfaces;
using PhoneStoreManager.Services.Interfaces;
using System;
using System.Collections.Generic;

namespace PhoneStoreManager.Services
{
    public class ManufacturerService:IManufacturerService
    {
        private IManufacturerRepository _manufacturerRepository;

        public ManufacturerService(IManufacturerRepository manufacturerRepository)
        {
            _manufacturerRepository = manufacturerRepository;
        }

        public void Add(Manufacturer manufacturer)
        {
            manufacturer.SearchField = (manufacturer.Name + " " + manufacturer.Address).ToUpper();
            _manufacturerRepository.Add(manufacturer);
        }

        public Manufacturer GetById(int id)
        {
            return _manufacturerRepository.GetById(id);
        }

        public Manufacturer FindByName(string name)
        {
            return _manufacturerRepository.FindByName(name);
        }

        public List<Manufacturer> Search(string searchText)
        {
            return _manufacturerRepository.Search(searchText);
        }

        public List<Manufacturer> GetAllManufacturers()
        {
            return _manufacturerRepository.GetAllManufacturers();
        }

        public void Save(Manufacturer manufacturer)
        {
             manufacturer.SearchField = (manufacturer.Name + manufacturer.Address).ToUpper();
             _manufacturerRepository.Save(manufacturer);
        }

        public void Remove(int id)
        {
             _manufacturerRepository.Remove(id);
        }
    }
}
