﻿using PhoneStoreManager.Domain;
using PhoneStoreManager.Repositories.Interfaces;
using PhoneStoreManager.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace PhoneStoreManager.Services
{
    public class PhoneColorService : IPhoneColorService
    {
        private IPhoneColorRepository _phoneColorRepository;

        public PhoneColorService(IPhoneColorRepository phoneColorRepository)
        {
            _phoneColorRepository = phoneColorRepository;
        }

        public List<PhoneColor> GetPhoneColors(int id)
        {
            return _phoneColorRepository.GetPhoneColors(id);
        }
    }
}
