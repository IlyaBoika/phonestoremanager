﻿using PhoneStoreManager.Domain;
using PhoneStoreManager.Repositories.Interfaces;
using PhoneStoreManager.Services.Interfaces;
using System.Collections.Generic;

namespace PhoneStoreManager.Services
{
    public class PhoneOSService : IPhoneOSService
    {

        private IPhoneOSRepository _phoneOSRepository;

        public PhoneOSService(IPhoneOSRepository phoneOSRepository)
        {
            _phoneOSRepository = phoneOSRepository;
        }


        public List<PhoneOS> GetAllPhoneOS()
        {
            return _phoneOSRepository.GetAllPhoneOS();
        }
    }
}
