﻿using PhoneStoreManager.Domain;
using PhoneStoreManager.Repositories.Interfaces;
using PhoneStoreManager.Services.Interfaces;
using System.Collections.Generic;

namespace PhoneStoreManager.Services
{
    public class PhoneService : IPhoneService
    {
        private IPhoneRepository _phoneRepository;

        public PhoneService(IPhoneRepository phonerepository)
        {
            _phoneRepository = phonerepository;

        }
        public void Add(Phone phone)
        {
            phone.SearchField = (phone.Name + " " + phone.Description).ToUpper();
            _phoneRepository.Add(phone);
        }

        public List<Phone> GetAllPhones()
        {
            return _phoneRepository.GetAllPhones();
        }

        public Phone GetById(int id)
        {
            return _phoneRepository.GetById(id);
        }

        public void Remove(int id)
        {
            _phoneRepository.Remove(id);
        }

        public void Save(Phone phone)
        {
            phone.SearchField = (phone.Name + " " + phone.Description).ToUpper();
            _phoneRepository.Save(phone);
        }

        public List<Phone> Search(string searchText)
        {
            return _phoneRepository.Search(searchText);
        }
    }
}