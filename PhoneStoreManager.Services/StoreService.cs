﻿using PhoneStoreManager.Domain;
using PhoneStoreManager.Repositories.Interfaces;
using PhoneStoreManager.Services.Interfaces;
using System.Collections.Generic;

namespace PhoneStoreManager.Services
{
    public class StoreService : IStoreService
    {
        private IStoreRepository _storeRepository;

        public StoreService(IStoreRepository storeRepository)
        {
            _storeRepository = storeRepository;
        }

        public void Save(Store store)
        {
            store.SearchField = (store.Address + " " + store.Name).ToUpper();
            _storeRepository.Save(store);
        }

        public List<Store> GetAllStores()
        {
            return _storeRepository.GetAllStores();
        }

        public Store GetById(int Id)
        {
            return _storeRepository.GetById(Id);
        }

        public Store FindByName(string name)
        {
            return _storeRepository.FindByName(name);
        }

        public List<Store> Search(string searchText)
        {
            return _storeRepository.Search(searchText);
        }

        public void AddPhoneToStore(int storeId, int phoneId, decimal price, int amount, int colorId)
        {
            _storeRepository.AddPhoneToStore(storeId, phoneId, price, amount, colorId);
        }

        public void Remove(int Id)
        {
            _storeRepository.Remove(Id);
        }

        public void RemovePhonesFromStore(int phoneId, int storeId)
        {
            _storeRepository.RemovePhonesFromStore(phoneId, storeId);
        }
    }
}