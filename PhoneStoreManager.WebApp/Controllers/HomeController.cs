﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PhoneStoreManager.Services;
using PhoneStoreManager.Services.Interfaces;
using PhoneStoreManager.WebApp.Models;
using PhoneStoreManager.WebApp.Models.Home;
using System.Diagnostics;

namespace PhoneStoreManager.WebApp.Controllers
{
    public class HomeController : Controller
    {
        private IStoreService _storeService;
        private IPhoneService _phoneService;
        private IManufacturerService _manufacturerService;

        public HomeController(IStoreService storeService, IPhoneService phoneService,
            IManufacturerService manufacturerService)
        {
            _storeService = storeService;
            _phoneService = phoneService;
            _manufacturerService = manufacturerService;
        }

        public IActionResult Index()
        {
            var manufacturers = _manufacturerService.GetAllManufacturers();
            var phones = _phoneService.GetAllPhones();

            var model = new ResultsModel
            {
                Filter = new FilterModel
                {
                    Manufacturers = manufacturers
                },

                Results = new FilteredPhonesModel
                {
                    Phones = phones
                }
            };

            return View(model);
        }


        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
