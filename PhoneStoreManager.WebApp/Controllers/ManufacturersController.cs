﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using PhoneStoreManager.Services.Interfaces;
using PhoneStoreManager.Domain;
using PhoneStoreManager.WebApp.Models.Manufacturers;
using AutoMapper;
using PhoneStoreManager.WebApp.Dto;

namespace PhoneStoreManager.WebApp.Controllers
{
    public class ManufacturersController : Controller
    {
        private IManufacturerService _manufacturerService;

        private readonly IMapper _mapper;

        public ManufacturersController(IManufacturerService phoneService, IMapper mapper)
        {
            _manufacturerService = phoneService;

            _mapper = mapper;
        }

        public IActionResult Index(string searchText)
        {
            List<Manufacturer> manufacturers = null;

            if (string.IsNullOrWhiteSpace(searchText))
            {
                manufacturers = _manufacturerService.GetAllManufacturers();
            }
            else
            {
                manufacturers = _manufacturerService.Search(searchText);
            }

            var model = new ManufacturersModel()
            {
                Manufacturers = _mapper.Map<List<ManufacturerDto>>(manufacturers),
                SearchText = searchText
            };

            return View(model);
        }

        [HttpGet]
        public IActionResult Edit(int Id)
        {
            ViewData["ActionType"] = "Edit";
            var manufacturer = _manufacturerService.GetById(Id);
            return View(manufacturer);
        }

        [HttpPost]
        public IActionResult Edit(Manufacturer manufacturer)
        {
            _manufacturerService.Save(manufacturer);
            return RedirectToAction("Index");
        }

        [HttpGet]
        public IActionResult Create()
        {
            ViewData["ActionType"] = "Create";
            return View("Edit", new Manufacturer());
        }

        [HttpPost]
        public IActionResult Create(Manufacturer manufacturer)
        {
            _manufacturerService.Save(manufacturer);
            return RedirectToAction("Index");
        }

        [HttpPost]
        public IActionResult Remove(int id)
        {
            _manufacturerService.Remove(id);
            return RedirectToAction("Index");
        }
    }
}