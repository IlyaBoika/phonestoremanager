﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using PhoneStoreManager.Domain;
using PhoneStoreManager.Services.Interfaces;
using PhoneStoreManager.WebApp.Dto;
using PhoneStoreManager.WebApp.Models.Phones;
using System.Collections.Generic;

namespace PhoneStoreManager.WebApp.Controllers
{
    public class PhonesController : Controller
    {
        private IPhoneService _phoneService;
        private IManufacturerService _manufacturerService;
        private IPhoneOSService _phoneOSService;
        private IPhoneColorService _phoneColorService;
        private readonly IMapper _mapper;

        public PhonesController(IPhoneService phoneService, IManufacturerService manufacturerService, IPhoneOSService phoneOSService, IPhoneColorService phoneColorService, IMapper mapper)
        {
            _phoneService = phoneService;
            _manufacturerService = manufacturerService;
            _phoneOSService = phoneOSService;
            _phoneColorService = phoneColorService;
            _mapper = mapper;
        }

        [HttpGet]
        public IActionResult Index(string searchText)
        {
            List<Phone> phones = null;

            if (string.IsNullOrWhiteSpace(searchText))
            {
                phones = _phoneService.GetAllPhones();
            }
            else
            {
                phones = _phoneService.Search(searchText);
            }

            var model = new PhonesModel()
            {
                Phones = _mapper.Map<List<PhoneDto>>(phones),
                SearchText = searchText
            };

            return View(model);
        }

        [HttpGet]
        public IActionResult Edit(int Id)
        {
            ViewData["ActionType"] = "Edit";

            var phone = _phoneService.GetById(Id);
            var manufactures = _manufacturerService.GetAllManufacturers();
            //var phoneOSes = _phoneService.GetAllPhones().Select(_ => _.PhoneOS).ToList();
            var phoneOSes = _phoneOSService.GetAllPhoneOS();

            var model = new EditPhoneModel
            {
                Phone = _mapper.Map<PhoneDto>(phone),
                Manufacturers = _mapper.Map<List<ManufacturerDto>>(manufactures),
                PhoneOSes = _mapper.Map<List<PhoneOSDto>>(phoneOSes)
            };

            return View(model);
        }

        [HttpPost]
        public IActionResult Edit(EditPhoneModel model)
        {
            Phone phone = _mapper.Map<Phone>(model.Phone);
            _phoneService.Save(phone);
            return RedirectToAction("Index");
        }

        [HttpGet]
        public IActionResult Create()
        {
            ViewData["ActionType"] = "Create";
            var manufactures = _manufacturerService.GetAllManufacturers();
            var phoneOSes = _phoneOSService.GetAllPhoneOS();

            var model = new EditPhoneModel
            {
                Phone = new PhoneDto(),
                Manufacturers = _mapper.Map<List<ManufacturerDto>>(manufactures),
                PhoneOSes = _mapper.Map<List<PhoneOSDto>>(phoneOSes)
            };

            return View("Edit", model);
        }

        [HttpPost]
        public IActionResult Create(EditPhoneModel model)
        {
            Phone phone = _mapper.Map<Phone>(model.Phone);
            _phoneService.Save(phone);
            return RedirectToAction("Index");
        }

        [HttpPost]
        public IActionResult Remove(int id)
        {
            _phoneService.Remove(id);
            return RedirectToAction("Index");
        }

        [HttpGet]
        public IActionResult GetPhoneColors(int id)
        {
            var colors = _phoneColorService.GetPhoneColors(id);

            return Ok(colors);
        }
    }
}