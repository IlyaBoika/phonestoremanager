﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using PhoneStoreManager.Domain;
using PhoneStoreManager.Services.Interfaces;
using PhoneStoreManager.WebApp.Dto;
using PhoneStoreManager.WebApp.Models.Stores;
using System.Collections.Generic;

namespace PhoneStoreManager.WebApp.Controllers
{
    public class StoresController : Controller
    {
        private readonly IStoreService _storeService;
        private readonly IPhoneService _phoneService;
        private readonly IMapper _mapper;

        public StoresController(IStoreService storeService, IPhoneService phoneService, IMapper mapper)
        {
            _storeService = storeService;
            _phoneService = phoneService;
            _mapper = mapper;
        }

        public IActionResult Index(string searchText)
        {
            List<Store> stores = null;

            if (string.IsNullOrWhiteSpace(searchText))
            {
                stores = _storeService.GetAllStores();
            }
            else
            {
                stores = _storeService.Search(searchText);
            }

            var model = new StoresModel()
            {
                Stores = _mapper.Map<List<StoreDto>>(stores),
                SearchText = searchText
            };

            return View(model);
        }

        [HttpGet]
        public IActionResult Edit(int id)
        {
            ViewData["ActionType"] = "Edit";
            var model = BuildEditStoreModel(id);

            return View(model);
        }

        [HttpPost]
        public IActionResult Edit(EditStoreModel model)
        {
            Store store = _mapper.Map<Store>(model.Store);
            _storeService.Save(store);
            return RedirectToAction("Index");
        }

        [HttpPost]
        public IActionResult AddPhoneToStore(EditStoreModel model)
        {
            ViewData["ActionType"] = "Edit";
            _storeService.AddPhoneToStore(model.AddPhoneToStoreModel.StoreId, model.AddPhoneToStoreModel.PhoneId, model.AddPhoneToStoreModel.Price, model.AddPhoneToStoreModel.Amount, model.AddPhoneToStoreModel.ColorId);
            return RedirectToAction("Edit", new { id = model.AddPhoneToStoreModel.StoreId });
        }

        [HttpGet]
        public IActionResult Create()
        {
            ViewData["ActionType"] = "Create";

            var phones = _phoneService.GetAllPhones();
            var store = new Store();

            var model = new EditStoreModel()
            {
                Store = new StoreDto(),
                StorePhonesModel = new StorePhonesModel
                {
                    StorePhones = _mapper.Map<List<StorePhoneDto>>(store.StorePhones),
                }
            };

            return View("Edit", model);
        }

        [HttpPost]
        public IActionResult Create(EditStoreModel model)
        {
            Store store = _mapper.Map<Store>(model.Store);
            _storeService.Save(store);
            return RedirectToAction("Index");
        }

        [HttpPost]
        public IActionResult Remove(int id)
        {
            _storeService.Remove(id);
            return RedirectToAction("Index");
        }

        [HttpPost]
        public IActionResult RemovePhonesFromStore(int phoneId, int storeId)
        {
            _storeService.RemovePhonesFromStore(phoneId, storeId);
            return RedirectToAction("Edit", new { id = storeId });
        }

        private EditStoreModel BuildEditStoreModel(int storeId)
        {
            var store = _storeService.GetById(storeId);
            var phones = _phoneService.GetAllPhones();

            return new EditStoreModel()
            {
                Store = _mapper.Map<StoreDto>(store),
                AddPhoneToStoreModel = new AddPhoneToStoreModel
                {
                    Phones = _mapper.Map<List<PhoneDto>>(phones),
                    StoreId = store.Id,
                    Price = 0,
                    Amount = 0
                },

                StorePhonesModel = new StorePhonesModel
                {
                    StorePhones = _mapper.Map<List<StorePhoneDto>>(store.StorePhones),
                }
            };
        }
    }
}