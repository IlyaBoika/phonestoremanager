﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using PhoneStoreManager.EntityFramework;
using PhoneStoreManager.Repositories;
using PhoneStoreManager.Repositories.Interfaces;
using PhoneStoreManager.Services;
using PhoneStoreManager.Services.Interfaces;
using PhoneStoreManager.WebApp.Mapping;

namespace PhoneStoreManager.WebApp.DependencyInjection
{
    public class IoCRegistrator
    {
        public static void Register(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            services.AddDbContext<PhoneStoreManagerDbContext>(options => options.UseSqlServer(Constants.ConnectionString));

            services.AddScoped<IStoreRepository, EFStoreRepository>();
            services.AddScoped<IStoreService, StoreService>();
            services.AddScoped<IPhoneRepository, EFPhoneRepository>();
            services.AddScoped<IPhoneService, PhoneService>();
            services.AddScoped<IManufacturerRepository, EFManufacturerRepository>();
            services.AddScoped<IManufacturerService, ManufacturerService>();
            services.AddScoped<IPhoneOSRepository, EFPhoneOSRepository>();
            services.AddScoped<IPhoneOSService, PhoneOSService>();
            services.AddScoped<IPhoneColorRepository, EFPhoneColorRepository>();
            services.AddScoped<IPhoneColorService, PhoneColorService>();

            services.AddScoped(_ => AutomapperConfigurator.Initialize());
        }
    }
}