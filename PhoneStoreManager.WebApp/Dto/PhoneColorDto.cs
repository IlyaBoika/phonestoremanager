﻿namespace PhoneStoreManager.WebApp.Dto
{
    public class PhoneColorDto
    {
        public int Id { get; set; }

        public int PhoneId { get; set; }

        public string Color { get; set; }

        public string Code { get; set; }
    }
}