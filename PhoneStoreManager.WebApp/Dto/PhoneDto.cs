﻿using PhoneStoreManager.Domain;

namespace PhoneStoreManager.WebApp.Dto
{
    public class PhoneDto
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public int ManufacturerId { get; set; }

        public string SearchField { get; set; }

        public string Description { get; set; }

        public decimal ScreenSize { get; set; }

        public int OSId { get; set; }

        public PhoneOSDto PhoneOS { get; set; }

        public ManufacturerDto Manufacturer { get; set; }
    }
}