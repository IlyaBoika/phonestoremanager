﻿namespace PhoneStoreManager.WebApp.Dto
{
    public class PhoneOSDto
    {
        public int Id { get; set; }

        public string OS { get; set; }
    }
}
