﻿namespace PhoneStoreManager.WebApp.Dto
{
    public class StoreDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string SearchField { get; set; }
    }
}
