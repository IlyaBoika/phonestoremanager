﻿namespace PhoneStoreManager.WebApp.Dto
{
    public class StorePhoneDto
    {
        public int StoreId { get; set; }
        public int PhoneId { get; set; }
        public decimal Price { get; set; }
        public int Amount { get; set; }
        public int ColorId { get; set; }

        public PhoneDto Phone { get; set; }
        public PhoneColorDto PhoneColor { get; set; }
    }
}