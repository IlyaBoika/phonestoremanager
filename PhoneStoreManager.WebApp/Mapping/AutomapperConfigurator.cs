﻿using AutoMapper;
using PhoneStoreManager.WebApp.Mapping.Profiles;

namespace PhoneStoreManager.WebApp.Mapping
{
    public class AutomapperConfigurator
    {
        public static IMapper Initialize()
        {
            var config = new MapperConfiguration(cfg => {
                cfg.AddProfiles(typeof(PhoneDtoProfile).Assembly);
            });

            return new Mapper(config);
        }
    }
}