﻿using AutoMapper;
using PhoneStoreManager.Domain;
using PhoneStoreManager.WebApp.Dto;

namespace PhoneStoreManager.WebApp.Mapping.Profiles
{
    public class PhoneDtoProfile: Profile
    {
        public PhoneDtoProfile()
        {
            CreateMap<Phone, PhoneDto>().ReverseMap()
                .ForMember(_ => _.Id, opt => opt.MapFrom(s => s.Id));

            CreateMap<StorePhone, StorePhoneDto>().ReverseMap();

            CreateMap<PhoneColor, PhoneColorDto>().ReverseMap();
        }
    }
}