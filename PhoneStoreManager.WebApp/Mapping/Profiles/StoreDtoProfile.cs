﻿using AutoMapper;
using PhoneStoreManager.Domain;
using PhoneStoreManager.WebApp.Dto;

namespace PhoneStoreManager.WebApp.Mapping.Profiles
{
    public class StoreDtoProfile: Profile
    {
        public StoreDtoProfile()
        {
            CreateMap<Store, StoreDto>().ReverseMap();
        }
  
    }
}