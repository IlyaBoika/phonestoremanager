﻿using PhoneStoreManager.Domain;
using System.Collections.Generic;

namespace PhoneStoreManager.WebApp.Models.Home
{
    public class ResultsModel
    {
        public FilterModel Filter { get; set; }
        public FilteredPhonesModel Results { get; set; }
    }

    public class FilterModel
    {
        public List<Manufacturer> Manufacturers { get; set; }
    }

    public class FilteredPhonesModel
    {
        public List<Phone> Phones { get; set; }
    }
}