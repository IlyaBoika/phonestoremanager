﻿using PhoneStoreManager.Domain;
using PhoneStoreManager.WebApp.Dto;
using System.Collections.Generic;

namespace PhoneStoreManager.WebApp.Models.Manufacturers
{
    public class ManufacturersModel
    {
        public List<ManufacturerDto> Manufacturers { get; set; }
        public string SearchText { get; set; }
    }
}
