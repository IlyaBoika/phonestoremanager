﻿using PhoneStoreManager.WebApp.Dto;
using System.Collections.Generic;

namespace PhoneStoreManager.WebApp.Models.Phones
{
    public class EditPhoneModel
    {
        public PhoneDto Phone { get; set; }
        public List<ManufacturerDto> Manufacturers { get; set; }
        public List<PhoneOSDto> PhoneOSes { get; set; }
    }
}