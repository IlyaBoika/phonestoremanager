﻿using PhoneStoreManager.Domain;
using PhoneStoreManager.WebApp.Dto;
using System.Collections.Generic;

namespace PhoneStoreManager.WebApp.Models.Phones
{
    public class PhonesModel
    {
        public List<PhoneDto> Phones { get; set; }
        public string SearchText { get; set; }
    }
}
