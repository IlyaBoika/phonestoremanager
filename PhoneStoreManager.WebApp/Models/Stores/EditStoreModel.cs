﻿using PhoneStoreManager.WebApp.Dto;
using System.Collections.Generic;

namespace PhoneStoreManager.WebApp.Models.Stores
{
    public class EditStoreModel
    {
        public StoreDto Store { get; set; }
        public AddPhoneToStoreModel AddPhoneToStoreModel { get; set; }
        public StorePhonesModel StorePhonesModel { get; set; }
    }

    public class AddPhoneToStoreModel
    {
        public List<PhoneDto> Phones { get; set; }
        public int StoreId { get; set; }
        public int PhoneId { get; set; }
        public int ColorId { get; set; }
        public decimal Price { get; set; }
        public int Amount { get; set; }
    }

    public class StorePhonesModel
    {
        public List<StorePhoneDto> StorePhones { get; set; }
    }
}