﻿using PhoneStoreManager.WebApp.Dto;
using System.Collections.Generic;

namespace PhoneStoreManager.WebApp.Models.Stores
{
    public class StoresModel
    {
        public string SearchText { get; set; }
        public List<StoreDto> Stores { get; set; }
 
    }
}